//
//  SceneDelegate.h
//  ptracetest
//
//  Created by Seo Hyun-gyu on 2023/07/09.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end


//
//  ViewController.m
//  ptracetest
//
//  Created by Seo Hyun-gyu on 2023/07/09.
//

#import "ViewController.h"
#import "dlfcn.h"
#import <sys/sysctl.h>

#define    PT_DENY_ATTACH    31

@interface ViewController ()

@end

typedef int (*ptrace_ptr_t)(int _request, pid_t _pid, caddr_t _addr, int _data);

int anti_debug(void) {
    ptrace_ptr_t ptrace_ptr = (ptrace_ptr_t)dlsym(RTLD_SELF, "ptrace");
    return ptrace_ptr(PT_DENY_ATTACH,0,0,0);
}
void svc80_anti_debug(void) {
#if defined __arm64__
    __asm __volatile("mov x0, #26");
    __asm __volatile("mov x1, #31");
    __asm __volatile("mov x2, #0");
    __asm __volatile("mov x3, #0");
    __asm __volatile("mov x16, #0");
    __asm __volatile("svc #0x80");
#endif
}

static bool AmIBeingDebugged(void)
    // Returns true if the current process is being debugged (either
    // running under the debugger or has a debugger attached post facto).
{
    int                 junk;
    int                 mib[4];
    struct kinfo_proc   info;
    size_t              size;

    // Initialize the flags so that, if sysctl fails for some bizarre
    // reason, we get a predictable result.

    info.kp_proc.p_flag = 0;

    // Initialize mib, which tells sysctl the info we want, in this case
    // we're looking for information about a specific process ID.

    mib[0] = CTL_KERN;
    mib[1] = KERN_PROC;
    mib[2] = KERN_PROC_PID;
    mib[3] = getpid();

    // Call sysctl.

    size = sizeof(info);
    junk = sysctl(mib, sizeof(mib) / sizeof(*mib), &info, &size, NULL, 0);
    assert(junk == 0);

    // We're being debugged if the P_TRACED flag is set.

    return ( (info.kp_proc.p_flag & P_TRACED) != 0 );
}

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (IBAction)get_ppid:(id)sender {
    pid_t ppid = getppid();
    [sender setTitle:[NSString stringWithFormat:@"ppid: %d", ppid] forState:UIControlStateNormal];
    [sender setTintColor:UIColor.grayColor];
}

- (IBAction)check_debug_sysctl:(UIButton *)sender {
    bool debugged = AmIBeingDebugged();
    [sender setTitle:[NSString stringWithFormat:@"sysctl AmIBeingDebugged ret: %d", debugged] forState:UIControlStateNormal];
    [sender setTintColor:UIColor.grayColor];
}

- (IBAction)do_ptrace:(UIButton *)sender {
    svc80_anti_debug();
    
    int ret = anti_debug();
    [sender setTitle:[NSString stringWithFormat:@"ptrace ret: %d", ret] forState:UIControlStateNormal];
    [sender setTintColor:UIColor.grayColor];
}


@end

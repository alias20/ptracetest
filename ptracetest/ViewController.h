//
//  ViewController.h
//  ptracetest
//
//  Created by Seo Hyun-gyu on 2023/07/09.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
- (IBAction)do_ptrace:(id)sender;
- (IBAction)check_debug_sysctl:(id)sender;
- (IBAction)get_ppid:(id)sender;


@end

